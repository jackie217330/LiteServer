const pool = {};
module.exports = (key = '', initial = a => a) => {
    return (pool[key] =
        pool[key] || (typeof initial === 'function' ? initial() : initial));
};
