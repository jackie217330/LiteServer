## 更新日誌
### Version 1.0.4
- 修改middlewares載入順序
- 修改koa-logger內容

### Version 1.0.3
- 修正controller載入問題

### Version 1.0.2
- 整合koa-static
- 呼叫controller功能時傳入請求資料

### Version 1.0.1
- 支援Oauth

### Version 1.0.0
- 基本接口