const path = require("path"),
    Router = require("koa-router"),
    fs = require("fs");
const router = new Router();
const __dir = process.cwd();

/**
 * An Instance to collect rules, For display and test purpose
 * @type {Array}
 */
const loaded = [];

/**
 * Overloader
 * @param args
 * @returns {Array|Router}
 */
module.exports = function(...args) {
    return args.length ? getMiddleware(...args) : loaded;
};

function getMiddleware({ controller = { disabled: false } }) {
    if (controller.disabled) return async (ctx, next) => await next();
    return init(controller).routes();
}

/**
 * Router Initiate Function
 * @param root {String} Where the Controllers locate
 * @param methods {Array<String>} The Allow Methods
 * @returns {Router} Koa Router
 */
function init({
    root = "./controller",
    methods = ["get", "post", "put", "delete"],
    invalidPageError = true
}) {
    let abs_full_path = path.join(__dir, root);

    fsdir(abs_full_path).map(file => {
        file = path.relative(abs_full_path, file);

        try {
            let location = `/${file
                    .replace(/\.js$/, "")
                    .replace(/\\/g, "/")
                    .replace(/index$/, "")}`,
                _Module = require(path.join(abs_full_path, location)),
                Module = _Module.default || _Module,
                module = new Module();

            methods.forEach(method => {
                if (typeof module[method] === "function") {
                    location = location.replace(/\/_/g, "/:");
                    router[method](
                        location,
                        getMiddlewareFunction(module, method)
                    );
                    loaded.push({ method, location });
                }
            });
        } catch (e) {
            console.info("Module Not Loaded:", file);
        }
    });
    if (invalidPageError)
        methods.forEach(method => {
            router[method]("*", async ctx => {
                throw "invalid page";
            });
        });
    return router;
}

/**
 *
 * @param module
 * @param method
 * @returns {function(*)} Middleware function for Koa
 */
function getMiddlewareFunction(module, method) {
    return async ctx => {
        module.ctx = ctx;
        let jsonData = null;
        try {
            jsonData = JSON.parse(ctx.request.body || "{}");
        } catch (e) {}
        module.params = Object.assign(
            {},
            ctx.params,
            ctx.request.query,
            jsonData ? jsonData : ctx.request.body.fields || ctx.request.body
        );
        module.files = ctx.request.body.files || [];
        return await module[method](module.params, module.ctx, module.files);
    };
}
/**
 * Read files under folder recursively
 * @param dir {String}
 * @returns {Array<String>}
 */
function fsdir(dir) {
    if (!fs.lstatSync(dir).isDirectory()) return [dir];
    return fs
        .readdirSync(dir)
        .reduce(
            (reducer, file) => [...reducer, ...fsdir(`${dir}/${file}`)],
            []
        );
}
