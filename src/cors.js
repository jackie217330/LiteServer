const Cors = require('koa2-cors');
module.exports = ({ cors = { disabled: true, options: undefined } }) => {
    if (cors.disabled) return async (ctx, next) => await next();
    return cors.options ? Cors(cors.options) : Cors();
};