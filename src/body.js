const Body = require('koa-body');

module.exports = ({ body = { disabled: false, multipart: true } }) => {
    if (body.disabled) return async (ctx, next) => await next();
    return Body(body);
};
