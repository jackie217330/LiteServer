const Session = require('koa-session'),
    low = require('lowdb'),
    singleton = require('../singleton'),
    FileSync = require('lowdb/adapters/FileSync');
let db;

const lowStore = {
    get: key => {
        prepare();
        let sess = db.get(key).value();
        if (!sess) return {};
        else if (Date.now() > sess._expire)
            return db
                .set(key, {})
                .write()
                .value();
        else return sess;
    },
    set: (key, sess, maxAage, { changed }) => {
        prepare();
        if (changed) db.set(key, sess).write();
    },
    destroy: key => {
        prepare();
        db.remove(key).write();
    }
};

module.exports = ({ session = {} }, app) => {
    if (session.disabled) return async (ctx, next) => await next();
    return parse(session, app);
};

function parse(opts, app) {
    opts = {
        app_keys: ['LiteServer2018'],
        maxAge: 86400000,
        overwrite: true,
        httpOnly: true,
        signed: true,
        rolling: false,
        renew: false,
        store: lowStore,
        ...opts
    };
    app.keys = opts.app_keys;
    return Session(opts, app);
}

function prepare() {
    db = singleton('session', () => low(new FileSync(process.cwd() + '/session.json')));
    return true;
}