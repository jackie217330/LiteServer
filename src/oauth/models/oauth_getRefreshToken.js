const db = require('../lowTable')('/oauth.json');

module.exports = function(refreshToken) {
    let result = db.find({ refreshToken });

    if (result)
        return Object.assign(result, {
            accessTokenExpiresAt: new Date(result.accessTokenExpiresAt),
            refreshTokenExpiresAt: new Date(result.refreshTokenExpiresAt)
        });

    return null;
};
