const db = require('../lowTable')('/user.json');

module.exports = function(username, password, callback) {
    let result = db.find({ username, password });

    if (result) callback(false, result.id);
    else callback(new Error('Invalid User'), null);
};
