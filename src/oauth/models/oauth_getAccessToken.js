const db = require('../lowTable')('/oauth.json');

module.exports = function(accessToken) {
    let result = db.find({ accessToken });

    if (result) {
        return Object.assign(result, {
            accessTokenExpiresAt: new Date(result.accessTokenExpiresAt),
            refreshTokenExpiresAt: new Date(result.refreshTokenExpiresAt)
        });
    }
    return null;
};
