const db = require('../lowTable')('/oauth.json');

module.exports = function(token, client, user) {
    if (db.find({ client, user })) return db.update({ client, user }, token);
    db.create(Object.assign(token, { client, user }));
    return token;
};