const low = require('lowdb'),
    singleton = require('../../singleton'),
    FileSync = require('lowdb/adapters/FileSync');

module.exports = (file = './user.json') => {
    let db, collection;

    return {
        find: where => (prepare() && collection.find(where).value()) || null,
        create: data => prepare() && collection.push(data).write()[0],
        update: (where, data) =>
            prepare() &&
            collection
                .find(where)
                .assign(data)
                .write(),
        delete: where => prepare() && collection.remove(where).write(),
        length: () => prepare() && collection.size().value(),
        last: () => prepare() && collection.takeRight(1).value()[0],
        collection: () => prepare() && collection
    };

    function prepare() {
        db = singleton(file, () =>
            low(new FileSync(process.cwd() + file.replace(/^\.\//, '/')))
        );
        collection = singleton(`${file}_`, () => {
            db.defaults({ data: [] }).write();
            return db.get('data');
        });
        return true;
    }
};
