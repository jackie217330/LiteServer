const OauthServer = require('oauth2-server'),
    anymatch = require('anymatch'),
    request = require('./request'),
    response = require('./response');

module.exports = ({ oauth = { disabled: true } }) => {
    if (oauth.disabled) return async (ctx, next) => await next();

    const oauthServer = new OauthServer(
        Object.assign(
            {
                grants: ['password', 'refresh_token'],
                debug: false
            },
            oauth,
            {
                model: Object.assign({}, require('./models'), oauth.model || {})
            }
        )
    );

    return async (ctx, next) => {
        let path = getPath(ctx);

        if (anymatch([oauth.at || '/token'], path)) {
            return await oauthServer.token(request(ctx), response(ctx));
        } else if (!anymatch(oauth.except || ['/'], path)) {
            let token = await oauthServer.authenticate(
                request(ctx),
                response(ctx)
            );
            ctx.token = token;
            return await next();
        }
        return await next();
    };
};

function getPath(ctx) {
    return ctx.request.url.replace(/\?.*$/, '');
}
